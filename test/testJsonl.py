# -*- coding: utf-8 -*-
from types import BooleanType, NoneType, FloatType, IntType, ListType, \
    StringType, UnicodeType
import copy
import jsonl
import simplejson as json
import unittest
try:
    from simplejson.decoder import JSONDecodeError
except:
    from simplejson.scanner import JSONDecodeError

class testJSONL_simpleTypesForKeysInvalid(unittest.TestCase):
    def testDictDump(self):
        try:
            {{}:123}
        except TypeError:
            assert True
        else:
            assert False

    def testDictLoad(self):
        tv = '{{}:123}'
        try:
            jsonl.loads(tv)
        except JSONDecodeError:
            assert True
        else:
            assert False

    def testList(self):
        try:
            {[]:123}
        except TypeError:
            assert True
        else:
            assert False


class testJSONL_simpleTypesForKeys(unittest.TestCase):
    def setUp(self):
        self.tv = {None:None,
                   False:False,
                   True:True,
                   1.5264:'a',
                   123:'b',
                   'c':456,
                   }

    def tearDown(self):
        pass

    def testBoolean(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.false == False
        assert type(r.false) == BooleanType
        assert r.true == True
        assert type(r.true) == BooleanType

    def testNone(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.null == None
        assert type(r.null) == NoneType

    def testFloat(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        attr = getattr(r, '1.5264')
        assert attr == 'a'
        assert type(attr) in [StringType, UnicodeType]

    def testInteger(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        attr = getattr(r, '123')
        assert attr == 'b'
        assert type(attr) in [StringType, UnicodeType]

class testJSONL_simpleTypesForValues(unittest.TestCase):
    def setUp(self):
        self.tv = {'null':None,
                   'false':False,
                   'true':True,
                   'a':1.5264,
                   'b':123,
                   'c':'ccc',
                   'd':[],
                   'e':{},
                   }

    def tearDown(self):
        pass

    def testBoolean(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.false == False
        assert type(r.false) == BooleanType
        assert r.true == True
        assert type(r.true) == BooleanType

    def testNone(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.null == None
        assert type(r.null) == NoneType

    def testFloat(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.a == 1.5264
        assert type(r.a) == FloatType

    def testInteger(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.b == 123
        assert type(r.b) == IntType

    def testList(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.d == []
        assert type(r.d) == ListType

    def testDict(self):
        a = jsonl.dumps(self.tv)
        r = jsonl.loads(a)
        assert r.e == {}
        assert r.e.__class__.__name__ == jsonl.OBJECT_NAME

class testJSONL_core(unittest.TestCase):
    def setUp(self):
        self.tv = '{"a":1}'
        self.tv1 = '{"a":{"b":2}, "c":3}'
        self.tv_ = {"count":1}
        self.tv1_ = {"a":{"b":2}, "c":[3]}
        self.tvn_ = {'null':1}
        self.tvn1_ = {None:None, False:False, True:True, 'a':float(1.5264), 'b':int(123), 'c':'ccc', 'd':[]}
        self.tv2 = {None:1, True:False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}

    def tearDown(self):
        pass

    def testLoadsNonString(self):
        tv = self.tv1_
        assert jsonl.isEqual(jsonl.loads(tv), jsonl.loads(tv))
        assert jsonl.loads(tv)==jsonl.loads(tv)
        assert jsonl.loads(tv)==jsonl.loads(tv)

    def testDumpMutableLoad1(self, tv=None):
        tv = tv or self.tv2
        a = jsonl.dumps(tv)
        b = jsonl.loads(a, __mutable__=True)
        c = jsonl.dumps(b)
        assert jsonl.isEqual(json.loads(a), json.loads(c))

    def testDumpImmutableLoad1(self, tv=None):
        tv = tv or self.tv2
        a = jsonl.dumps(tv)
        b = jsonl.loads(a, __mutable__=False)
        c = jsonl.dumps(b)
        assert jsonl.isEqual(json.loads(a), json.loads(c))

    def testCompare(self, tv=None):
        tv = tv or self.tv2
        a = jsonl.dumps(tv)
        for m in [True, False]:
            b = jsonl.loads(a, __mutable__=m)
            d = jsonl.loads(a, __mutable__=m)
            c = jsonl.dumps(b)
            assert jsonl.isEqual(json.loads(a), json.loads(c))
            assert jsonl.isEqual(a, c)
            assert jsonl.isEqual(b, d)
            assert jsonl.isEqual(c, d)

    def testSimpleAttribute(self):
        r = jsonl.loads(self.tv)
        assert r.a == 1

    def testComplexAttribute(self):
        r = jsonl.loads(self.tv1)
        assert r.a.b == 2
        assert r.c == 3

    def testLoadsImmutableNamedTuple(self):
        r = jsonl.loads(self.tv1, __mutable__=False)
        assert r.a.__class__.__name__ == jsonl.OBJECT_NAME
        for i in [r, r.a]:
            try:
                setattr(i, 'test', 1)
            except AttributeError:
                assert True
            else:
                assert False

    def testLoadsMutableObject(self):
        eV = 123
        r = jsonl.loads(self.tv1, __mutable__=True)
        assert r.a.__class__.__name__ == jsonl.OBJECT_NAME
        for i in [r, r.a]:
            try:
                setattr(i, 'test', eV)
            except Exception:
                assert False
            else:
                assert r.test == eV

    def testLoadsMutableObjectByDefault(self):
        r = jsonl.loads(self.tv1)
        assert r.a.__class__.__name__ == jsonl.OBJECT_NAME

    def testDumps(self):
        rd = jsonl.dumps(self.tv_)
        r = jsonl.loads(rd)
        assert r.count == 1

    def testComplexDumps(self):
        rd = jsonl.dumps(self.tv1_)
        r = jsonl.loads(rd)
        assert r.a.__class__.__name__ == jsonl.OBJECT_NAME
        assert r.a.b == 2
        assert r.c == [3]

    def testDumpsEquivelance(self):
        a = json.dumps(self.tv)
        b = jsonl.dumps(self.tv)
        assert a == b

    def testDumpsComplexEquivelance(self):
        assert json.dumps(self.tv1) == jsonl.dumps(self.tv1)

    def testDumpMutableLoad(self, tv=None):
        tv = tv or self.tv
        a = jsonl.dumps(tv)
        b = jsonl.loads(a, __mutable__=True)
        c = jsonl.dumps(b)
        assert a == c

    def testDumpImmutableLoad(self, tv=None):
        tv = tv or self.tv
        a = jsonl.dumps(tv)
        b = jsonl.loads(a, __mutable__=True)
        c = jsonl.dumps(b)
        assert a == c

    def testDumpMutableLoadComplex(self):
        return self.testDumpMutableLoad(tv=self.tv1_)

    def testDumpImmutableLoadComplex(self):
        return self.testDumpImmutableLoad(tv=self.tv1_)

    def getWebEx1(self):
        FN = 'firstName'
        FNV = 'John'
        LN = 'lastName'
        LNV = 'Smith'
        voo = '''{
     "%s": "%s",
     "%s": "%s",
     "age": 25,
     "ageAtBirth": "0",
     "address":
     {
         "streetAddress": "21 2nd Street",
         "city": "New York",
         "state": "NY",
         "postalCode": "10021"
     },
     "phoneNumber":
     [
         {
           "type": "home",
           "number": "212 555-1234"
         },
         {
           "type": "fax",
           "number": "646 555-4567"
         }
     ]
     }''' % (FN, FNV, LN, LNV)
        return (FN, FNV, LN, LNV), voo

    def testWebEx1(self):
        (_FN, FNV, _LN, LNV), voo = self.getWebEx1()
        def check(o):
            assert o.firstName == FNV
            assert o.lastName == LNV
            assert o.age == 25
            assert o.ageAtBirth == "0"
            assert o.address.streetAddress == "21 2nd Street"
            assert o.address.city == "New York"
            assert o.address.state == "NY"
            assert o.address.postalCode == "10021"
            assert o.phoneNumber[0].type == "home"
            assert o.phoneNumber[0].number == "212 555-1234"
            assert o.phoneNumber[1].type == "fax"
            assert o.phoneNumber[1].number == "646 555-4567"
        for m in [True, False]:
            l = jsonl.loads(voo, __mutable__=m)
            check(l)
            d = jsonl.dumps(l)
            assert jsonl.isEqual(l, d)
            for n in [True, False]:
                assert jsonl.isEqual(l, jsonl.loads(d, __mutable__=n))
                assert jsonl.isEqual(d, jsonl.loads(d, __mutable__=n))

    def testDumpMutableTransMutable(self, tv=None, tv2=None):
        tv = tv or self.tv_
        tv2 = tv2 or self.tv2
        a = jsonl.loads(jsonl.dumps(tv), __mutable__=True)
        b = jsonl.loads(jsonl.dumps(tv2), __mutable__=False)
        a.FiElD = b
        try:
            a.FiElD.c = b
        except AttributeError:
            assert True
        else:
            assert False
        c = jsonl.dumps(a)
        d = jsonl.loads(c)
        assert jsonl.isEqual(a, d)
        assert jsonl.isEqual(a, c)

    def testRecursive(self, tv=None):
        tv = tv or self.tv_
        a = jsonl.loads(jsonl.dumps(tv), __mutable__=True)
        #    Make recursive:
        a.FiElD = a
        try:
            jsonl.dumps(a)
        except jsonl.JSONDecodeCircularError as e:
            assert e.message == a
        else:
            assert False

    def testNonRecursive(self, tv=None):
        tv = tv or self.tv_
        a = jsonl.loads(jsonl.dumps(tv), __mutable__=True)
        a1 = jsonl.loads(jsonl.dumps(tv), __mutable__=True)
        #    Make recursive:
        a.FiElD = a1
        jsonl.dumps(a)

    def testRecursiveComplexAllMutable(self):
        _, voo = self.getWebEx1()
        a = jsonl.loads(voo, __mutable__=True)
        a2 = jsonl.loads(voo, __mutable__=True)
        a3 = jsonl.loads(voo, __mutable__=True)
        b = jsonl.loads(jsonl.dumps(self.tv2), __mutable__=False)
        for what in [a, a2, a3]:
            a.FiElD = a2
            a.FiElD.fIeLd = a3
            a.FiElD.fIeLd.fIELd = b
            #    Make recursive:
            a.FiElD.fIeLd.fIELd1 = what
            try:
                jsonl.dumps(a)
            except jsonl.JSONDecodeCircularError as e:
                assert True
            else:
                assert False

    def testImmutableCannotBeRecursive(self):
        _, voo = self.getWebEx1()
        a = jsonl.loads(voo, __mutable__=True)
        a2 = jsonl.loads(voo, __mutable__=True)
        a3 = jsonl.loads(voo, __mutable__=True)
        b = jsonl.loads(jsonl.dumps(self.tv2), __mutable__=False)
        a.FiElD = a2
        a.FiElD.fIeLd = a3
        #    Make recursive:
        a.FiElD.fIeLd.fIELd = b
        jsonl.dumps(a)

    def testEquivalenceDict(self):
        json_data = {'workmen':[{'name':'bob the plumber',
                                 'jobs':[123, 124, 125, 200, 211],
                                 'clients':{'name':'Mrs. Plumber',
                                            'age': 31,
                                            'occupation':'house wife',
                                            'payments': ['$10.99', '$98.20']
                                           }},
                                ],
                     'accounts':1,
                     'random_data':(1, 2, 3),
                     }
        data = jsonl.loads(json_data)
        a = repr(data)
        b = str(data)
        c = unicode(data)
        assert jsonl.isEqual(json_data, data)
        assert jsonl.isEqual(data, json_data)
        assert not jsonl.isEqual(a, json_data)
        assert jsonl.isEqual(b, data)
        assert jsonl.isEqual(c, data)

    def testEquivalenceList(self):
        json_data = [{'name':'bob the plumber',
                                 'jobs':[123, 124, 125, 200, 211],
                                 'clients':{'name':'Mrs. Plumber',
                                            'age': 31,
                                            'occupation':'house wife',
                                            'payments': ['$10.99', '$98.20']
                                           }}]
        data = jsonl.loads(json_data)
        a = repr(data)
        assert str(data)
        assert unicode(data)
        assert jsonl.isEqual(json_data, data)
        assert jsonl.isEqual(data, json_data)
        assert jsonl.isEqual(a, json_data)
        assert jsonl.isEqual(json_data, a)

class testJSONL_item(unittest.TestCase):
    def setUp(self):
        self.tv = '{"a":1}'
        self.tv1 = '{"a":{"b":2}, "c":3}'
        self.tv_ = {"count":1}
        self.tv1_ = {"a":{"b":2}, "c":[3]}
        self.tvn_ = {'null':1}
        self.tvn1_ = {None:1}
        self.tv2 = {None:1, True:False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}

    def tearDown(self):
        pass

    def testDelIntegerKey(self):
        tv = self.tv1_
        obj = jsonl.loads(tv)
        del obj[0]
        assert len(obj) == 1
        assert 'a' not in obj
        assert 'c' in obj
        del obj[0]
        assert len(obj) == 0
        assert 'a' not in obj
        assert 'c' not in obj
        try:
            del obj[0]
        except IndexError:
            assert True
        else:
            assert False

    def testDelIntegerKeyComplex(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj[0]
        assert len(obj) == 2
        assert 'a' not in obj
        assert 'null' in obj
        assert 'true' in obj
        del obj[0]
        assert len(obj) == 1
        assert 'a' not in obj
        assert 'null' not in obj
        assert 'true' in obj
        del obj[0]
        assert len(obj) == 0
        assert 'a' not in obj
        assert 'null' not in obj
        assert 'true' not in obj
        try:
            del obj[0]
        except IndexError:
            assert True
        else:
            assert False

    def testItemAccess(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        assert obj['null'] == 1
        assert obj['true'] == False
        assert isinstance(obj['a'], list)

    def testItemAccessNotExist(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            obj['doesnotexist']
        except IndexError:
            assert True
        else:
            assert False

    def testItemAssignmentByKey(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        obj['y'] = 789
        assert obj['y'] == 789
        assert obj['true'] == False
        obj['true'] = 654
        assert obj['true'] == 654

    def testItemAssignmentByIndex(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        eV1 = 789
        obj[1] = eV1
        assert obj[1] == eV1
        assert obj['null'] == eV1
        assert obj['true'] == False
        eV2 = 'yes!'
        obj[2] = eV2
        assert obj[2] == eV2
        assert obj['true'] == eV2
        assert obj[1] == eV1
        eV3 = [678]
        obj[0] = eV3
        assert obj[0] == eV3
        assert obj[2] == eV2
        assert obj['true'] == eV2
        assert obj[1] == eV1

    def testItemContains(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        assert 'true' in obj
        assert not 'b' in obj

    def testItemLength(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        assert len(obj) == 3
        assert len(obj['a']) == 1
        assert len(obj['a'][0].c) == 2

    def testItemDeletion(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        assert obj['a'] != None
        del obj['a']
        try:
            obj['a']
        except IndexError:
            assert True
        else:
            assert False
        assert obj['null'] == 1
        assert obj['true'] == False

    def testItemDeletionNotExist(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            obj['doesnotexist']
        except IndexError:
            assert True
        else:
            assert False

    def testItemAccessInvalidKey(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            obj['_a']
        except KeyError:
            assert True
        else:
            assert False

    def testItemAssignmentInvalidKey(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            obj['_a'] = 567
        except KeyError:
            assert True
        else:
            assert False

    def testItemDeletionInvalidKey(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            del obj['_a']
        except KeyError:
            assert True
        else:
            assert False

class testJSONL_slice(unittest.TestCase):
    def setUp(self):
        self.tv = '{"a":1}'
        self.tv1 = '{"a":{"b":2}, "c":3}'
        self.tv_ = {"count":1}
        self.tv1_ = {"a":{"b":2}, "c":[3]}
        self.tvn_ = {'null':1}
        self.tvn1_ = {None:1}
        self.tv2 = {None:1, True:False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}
        self.tv3 = {'hello':1, 'true':False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}

    def tearDown(self):
        pass

    def test(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        voo = obj[:]
        assert voo[1] == 1
        assert voo[2] == False
        assert str(obj[0:1][0]) == str(jsonl.loads(tv['a']))
        voo = obj[0:2]
        tv1 = copy.deepcopy(tv)
        tv1.pop(None)
        tv1.pop(True)
        assert str(obj[0:2][0]) == str(jsonl.loads(tv1['a']))

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[0:3]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['a'])
        assert str(x[1]) == str(xx['hello'])
        assert str(x[2]) == str(xx['true'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[1:2]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['hello'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[1:3]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['hello'])
        assert str(x[1]) == str(xx['true'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[2:3]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['true'])

        assert obj[-1:-1] == []
        assert obj[-2:-2] == []
        assert obj[-3:-3] == []

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[-2:-1]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['hello'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[-3:-1]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['a'])
        assert str(x[1]) == str(xx['hello'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[-3:-2]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['a'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[0:4]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['a'])
        assert str(x[1]) == str(xx['hello'])
        assert str(x[2]) == str(xx['true'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[1:4]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['hello'])
        assert str(x[1]) == str(xx['true'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[2:4]
        xx = jsonl.loads(tv)
        assert str(x[0]) == str(xx['true'])

        tv = copy.deepcopy(self.tv3)
        obj = jsonl.loads(tv)
        x = obj[4:4]
        xx = jsonl.loads(tv)
        assert x == []

    def testIndexOutOfRange(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        for stop in xrange(4, 10):
            assert obj[4:stop] == []
        assert obj[-4:-4] == []
        assert obj[4:4] == []

class testJSONL_sliceExtendedOperations(unittest.TestCase):
    def setUp(self):
        self.tv = '{"a":1}'
        self.tv1 = '{"a":{"b":2}, "c":3}'
        self.tv_ = {"count":1}
        self.tv1_ = {"a":{"b":2}, "c":[3]}
        self.tvn_ = {'null':1}
        self.tvn1_ = {None:1}
        self.tv2 = {None:1, True:False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}

    def tearDown(self):
        pass

    def testStepCannotBeZero(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            assert obj[::0] == ['a', 'null', 'true']
        except ValueError:
            assert True
        else:
            assert False

    def testStep(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        for start in [None, 0]:
            voo = obj[start::1]
            assert voo[0] == obj[0]
            assert voo[1] == obj[1]
            assert voo[2] == obj[2]

            voo = obj[start::2]
            assert voo[0] == obj[0]
            assert voo[1] == obj[2]

            voo = obj[start::3]
            assert voo[0] == obj[0]

            voo = obj[start::4]
            assert voo[0] == obj[0]

        voo = obj[1::1]
        assert voo[0] == obj[1]
        assert voo[1] == obj[2]
        for stop in xrange(2, 5):
            voo = obj[1::stop]
            assert voo[0] == obj[1]
        for stop in xrange(1, 5):
            voo = obj[2::stop]
            assert voo[0] == obj[2]

    def testStepReversed(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        voo = obj[::-1]
        assert voo[0] == tv[True]

        for stop in xrange(5):
            for step in xrange(1, 5):
                assert obj[0:stop:-step] == []

        voo = obj[::-2]
        assert voo[0] == tv[True]
        assert jsonl.dumps(jsonl.loads(str(voo[1][0]))) == jsonl.dumps(tv['a'][0])

        voo = obj[::-3]
        assert voo[0] == tv[True]

        voo = obj[::-4]
        assert voo[0] == tv[True]

    def testAssignmentInvalidStart(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            obj['1':2] = 3
        except ValueError:
            assert True
        else:
            assert False

    def testAssignmentInvalidStop(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            obj[1:'2':3]
        except ValueError:
            assert True
        else:
            assert False

    def testAssignmentInvalidStep(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            obj[1:2:'3']
        except ValueError:
            assert True
        else:
            assert False

class testJSONL_sliceExtendedAssignmentOperations(unittest.TestCase):
    def setUp(self):
        self.tv = '{"a":1}'
        self.tv1 = '{"a":{"b":2}, "c":3}'
        self.tv_ = {"count":1}
        self.tv1_ = {"a":{"b":2}, "c":[3]}
        self.tvn_ = {'null':1}
        self.tvn1_ = {None:1}
        self.tv2 = {None:1, True:False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}

    def tearDown(self):
        pass

    def testAssignmentValidStartSingleValue(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        obj['a':2] = 3
        assert obj['a'] == 3
        assert obj[0] == 3
        assert obj['null'] == 3
        assert obj[1] == 3
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        obj['a':1] = 4
        assert obj['a'] == 4
        assert obj[0] == 4
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        obj['a':0] = 5
        assert obj['a'] == 4
        assert obj[0] == 4
        assert obj[1] == 3
        assert obj[2] == False
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        obj = jsonl.loads(tv)
        obj['null':2] = 5
        assert obj['null'] == 5
        assert obj[1] == 5
        assert obj[2] == False
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        obj['null':1] = 5
        assert obj['null'] == 5
        assert obj[1] == 5
        assert obj[2] == False
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj

    def testAssignmentValidStartMultipleValues(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        eV = [3, 4]
        obj['a':2] = eV
        assert obj['a'] == eV
        assert obj[0] == eV
        assert obj['null'] == eV
        assert obj[1] == eV
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        eV1 = {'t':4, 'u':6}
        obj['a':1] = eV1
        assert obj['a'] == eV1
        assert obj[0] == eV1
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        obj['a':0] = 5
        assert obj['a'] == eV1
        assert obj[0] == eV1
        assert obj[1] == eV
        assert obj[2] == False
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        obj = jsonl.loads(tv)
        eV2 = jsonl.loads(tv)
        obj['null':2] = eV2
        assert obj['null'] == eV2
        assert obj[1] == eV2
        assert obj[2] == False
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj
        obj['null':2] = 5
        assert obj['null'] == 5
        assert obj[1] == 5
        assert obj[2] == False
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj

class testJSONL_sliceDelExtendedOperations(unittest.TestCase):
    def setUp(self):
        self.tv = '{"a":1}'
        self.tv1 = '{"a":{"b":2}, "c":3}'
        self.tv_ = {"count":1}
        self.tv1_ = {"a":{"b":2}, "c":[3]}
        self.tvn_ = {'null':1}
        self.tvn1_ = {None:1}
        self.tv2 = {None:1, True:False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}

    def tearDown(self):
        pass

    def testDelValidIntegers(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj[0:2:1]
        assert len(obj) == 1
        assert obj['true'] == False
        assert not 'a' in obj
        assert not 'null' in obj

    def testDelValidStrings1(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj['a':'null']
        assert len(obj) == 2
        assert obj['null'] == 1
        assert obj['true'] == False
        assert not 'a' in obj

    def testDelValidStrings2(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj['a':'true']
        assert len(obj) == 1
        assert not 'a' in obj
        assert not 'null' in obj
        assert 'true' in obj

    def testDelValidStrings3(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj['a':'true':1]
        assert len(obj) == 1
        assert not 'a' in obj
        assert not 'null' in obj
        assert 'true' in obj

    def testDelValidStrings4(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj['a':'true':2]
        assert len(obj) == 2
        assert not 'a' in obj
        assert 'null' in obj
        assert 'true' in obj

    def testDelValidStrings5(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj['a':'true':-1]
        assert len(obj) == 3
        assert 'a' in obj
        assert 'null' in obj
        assert 'true' in obj

    def testDelValidReversed(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj[2:0:-1]
        assert len(obj) == 1
        assert 'a' in obj
        assert not 'true' in obj
        assert not 'null' in obj

    def testDelValidNoSliceArgs(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        del obj[::]
        assert len(obj) == 0

    def testDelInvalidStart(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            del obj['voo':]
        except ValueError:
            assert True
        else:
            assert False

    def testDelInvalidStop(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            del obj[:'voo']
        except ValueError:
            assert True
        else:
            assert False

    def testDelInvalidStartAndStop(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        try:
            del obj['boo':'voo']
        except ValueError:
            assert True
        else:
            assert False

    def testDelInvalidStopComplex(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        for stop in ['s', 'boo', 'voo']:
            try:
                del obj['true':stop]
            except ValueError:
                assert True
            else:
                assert False

    def testDelInvalidStartComplex(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        for start in ['s', 'boo', 'voo']:
            try:
                del obj[start:'true']
            except ValueError:
                assert True
            else:
                assert False

    def testDelInvalidStep(self):
        tv = self.tv2
        obj = jsonl.loads(tv)
        for step in ['', 'true', 'voo']:
            try:
                del obj[0:1:step]
            except ValueError:
                assert True
            else:
                assert False

class testJSONL_iterjson(unittest.TestCase):
    def setUp(self):
        self.tv = '{"a":1}'
        self.tv1 = '{"a":{"b":2}, "c":3}'
        self.tv_ = {"count":1}
        self.tv1_ = {"a":{"b":2}, "c":[3]}
        self.tvn_ = {'null':1}
        self.tvn1_ = {None:1}
        self.tv2 = {None:1, True:False, 'a':[{'b':'bb', 'c':[1, {'f':'ff', 't':33}]}]}
        self.tv3 = [1, 2, {'a':'b'}]

    def tearDown(self):
        pass

    def testString(self):
        tv = self.tv1
        obj = jsonl.loads(tv)
        i = jsonl.iter(obj)
        assert sorted([m for m in i]) == ['a', 'c']
        i = jsonl.iterkeys(obj)
        assert sorted([m for m in i]) == ['a', 'c']

    def testDict(self):
        tv = self.tv1_
        i = jsonl.iter(tv)
        assert sorted([m for m in i]) == ['a', 'c']
        i = jsonl.iterkeys(tv)
        assert sorted([m for m in i]) == ['a', 'c']

    def testList(self):
        tv = self.tv3
        i = jsonl.iter(tv)
        assert sorted([m for m in i]) == tv
        i = jsonl.iterkeys(tv)
        assert sorted([m for m in i]) == tv

    def testJsonlObjectTransMutable(self):
        tv = self.tv1
        for m in [True, False]:
            obj = jsonl.loads(tv, __mutable__=m)
            i = jsonl.iter(obj)
            l = sorted([m for m in i])
            assert l == ['a', 'c']
            i = jsonl.iterkeys(obj)
            assert sorted([m for m in i]) == ['a', 'c']

    def testNonJsonlObjectOrString(self):
        tv = self.tv1_
        for f in ['iter', 'iterkeys']:
            i = getattr(jsonl, f)(tv)
            l = sorted([m for m in i])
            assert l == ['a', 'c']
        i = jsonl.itervalues(tv)
        l = sorted([m for m in i])
        assert l == [tv['a'], tv['c']]

    def testIterListAsValuesRaisesValueError(self):
        tv = self.tv3
        try:
            jsonl.itervalues(tv)
        except ValueError:
            assert True
        else:
            assert False

    def testIterDictAsValues(self):
        tv = self.tv1_
        i = jsonl.itervalues(tv)
        assert sorted([m for m in i]) == [tv['a'], tv['c']]

    def testIterJsonlObjectAsValues(self):
        tv = self.tv1_
        obj = jsonl.loads(tv)
        i = jsonl.itervalues(obj)
        a = sorted([m for m in i])
        for index, ev in enumerate([tv['a'], tv['c']]):
            assert jsonl.isEqual(jsonl.loads(ev), a[index])

    def testIterItemsOnDict(self):
        tv = self.tv1_
        i = jsonl.iteritems(tv)
        a = sorted([m for m in i])
        aa = sorted([m for m in tv.iteritems()])
        assert a == aa

    def testIterItemsOnListRisesValueError(self):
        tv = self.tv3
        try:
            jsonl.iteritems(tv)
        except ValueError:
            assert True
        else:
            assert False

    def testIterItemsJsonlObject(self):
        tv = self.tv1_
        obj = jsonl.loads(tv)
        i = jsonl.iteritems(obj)
        for k, v in sorted([m for m in i]):
            assert jsonl.isEqual(jsonl.loads(tv[k]), v)

    def testIterItemsNonJsonlObjectOrString(self):
        tv = self.tv1_
        i = jsonl.iteritems(tv)
        a = sorted([m for m in i])
        for k, v in a:
            assert tv[k] == v

    def testIter(self):
        tv = self.tv1
        obj = jsonl.loads(tv)
        assert [i for i in iter(obj)] == ['a', 'c']

class testJSONL_examples(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def test(self):
        json_data = {'workmen':[{'name':'bob the plumber',
                                 'jobs':[123, 124, 125, 200, 211],
                                 'clients':{'name':'Mrs. Plumber',
                                            'age': 31,
                                            'occupation':'house wife',
                                            'payments': ['$10.99', '$98.20']
                                           }},
                                ],
                     'accounts':1,
                     'data':'stuff',
                     'private':{'state':'sold'}
                     }
        #    Create the jsonl object.
        data = jsonl.loads(json_data)

        #    You can obtain a string or unicode representation of the jsonl object:
        a = repr(data)
        assert a.startswith('jsonl')
        print data
        print str(data)
        print unicode(data)
        #    Now let's play with the data:

        #    You can index it alphabetically:
        assert data[0] == 1
        #    or you can access via attributes:
        assert data['accounts'] == 1

        #    You can set attributes via an index:
        data[0] = 'hello world'
        assert data[0] == 'hello world'
        assert data['accounts'] == 'hello world'
        #    or via slicing:
        data[0:2] = 55    #    Assigns value to attributes 0,1
        assert len(data) == 4    #    num attributes unchanged.
        #    and extended slicing also:
        data[0::2] = 123
        assert data[0] == 123
        assert data[1] == 55
        assert data[2] == 123

        data = jsonl.loads(json_data)
        #    You can slice it to access the values using indexes:
        assert data[1:2] == ['stuff']
        assert data[0:2] == [1, 'stuff']
        #    or via keys: start or end
        assert str(data['accounts':1]) == str([1])
        assert data[0:'private'] == [1, unicode('stuff')]
        #    or via keys: start and end
        voo = data['accounts':'private']
        assert voo[0] == 1
        assert voo[1] == unicode('stuff')
        #    Use the slice step parameter as normal (integer or None)
        assert data[-3::-1] == ['stuff', 1]
        print data['private':'data':-1]
        assert str(data['private':'data':-1][0]) == str(jsonl.loads(json_data['private']))

        #    You can test for a key being 'in' the jsonl object's keys:
        assert 'accounts' in data
        assert 'doesntexist' not in data

        #    You can create an iterator from it:
        data = jsonl.loads(json_data)
        eKeys = ['accounts', 'data', 'private', 'workmen']
        #    This will iterate over the keys:
        assert [i for i in iter(data)] == eKeys
        #    Create an implicit custom iterator over the keys:
        assert [i for i in jsonl.iter(data)] == eKeys
        #    Create an explicit custom iterator over the values:
        assert [i for i in jsonl.iterkeys(data)] == eKeys
        #    Create an explicit custom iterator over the values:
        values = [i for i in jsonl.itervalues(data)]
        assert values[0] == json_data['accounts']
        assert values[1] == json_data['data']
        assert jsonl.isEqual(values[2], json_data['private'])
        assert jsonl.isEqual(values[3], json_data['workmen'])

        #    You can test for equality:
        data = jsonl.loads(json_data)
        data1 = jsonl.loads(json_data)
        #    Using the jsonl object
        assert jsonl.isEqual(data, json_data)
        assert jsonl.isEqual(data, data1)
        #    jsonl.dump
        assert jsonl.isEqual(data, jsonl.dumps(json_data))
        #    Add an attribute and check for inequality:
        data.aaaa = 1
        assert not jsonl.isEqual(data, data1)

        #    You can delete attributes via an index:
        data = jsonl.loads(json_data)
        del data[0]
        assert data[0] != 'hello world'
        assert 'accounts' not in data
        try:
            data['accounts']
        except IndexError as e:
            assert e.message == 'accounts'
        else:
            assert False
        #    or via keys:
        del data['workmen']
        assert 'workmen' not in data
        try:
            data['workmen']
        except IndexError as e:
            assert e.message == 'workmen'
        else:
            assert False

        #    You can get the length (number of keys) in the jsonl object:
        data = jsonl.loads(json_data)
        assert len(data) == 4 == len([i for i in iter(data)])

        #    You can also check for an accidental circular dependency:
        jsonl.verify(data)
        #    Add the circular dependency:
        data.workmen[0].voo = data
        #    And check - catching an exception:
        try:
            jsonl.verify(data)
        except jsonl.JSONDecodeCircularError:
            assert True
        else:
            assert False
        #    or without raising an exception
        assert not jsonl.verify(data, noraise=True)

class testJSONL_exampless(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def test(self):
        json_data = {'workmen':[{'name':'bob the plumber',
                                 'jobs':[123, 124, 125, 200, 211],
                                 'clients':{'name':'Mrs. Plumber',
                                            'age': 31,
                                            'occupation':'house wife',
                                            'payments': ['$10.99', '$98.20']
                                           }},
                                ],
                     'accounts':1,
                     'data':'stuff',
                     'private':{'state':'sold'}
                     }
        #    Create the jsonl object.
        data = jsonl.loads(json_data)
        if len(data.workmen)<2:
            data.private['count'] = 1
            data.accounts += 1
            del data.workmen[0].jobs[-1]
            data.workmen[0].clients.payments.append('$9.01')
            data[0:3:2] = 'invalidate'
            print jsonl.dumps(data)
            for k, v in jsonl.iteritems(data):
                print k, v

if __name__ == '__main__':
    unittest.main()
