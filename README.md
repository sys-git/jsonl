![Codeship Status](https://www.codeship.io/projects/2c7d9580-8132-0131-5ee2-7a0350787acb/status)

[![Downloads](https://pypip.in/download/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![Latest Version](https://pypip.in/version/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![Supported Python versions](https://pypip.in/py_versions/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![Supported Python implementations](https://pypip.in/implementation/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![Development Status](https://pypip.in/status/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![Wheel Status](https://pypip.in/wheel/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![Egg Status](https://pypip.in/egg/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![Download format](https://pypip.in/format/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)
[![License](https://pypip.in/license/jsonl/badge.svg)](https://pypi.python.org/pypi/jsonl/)


jsonl:  JSON loader which overrides the default JSON module's load and loads methods.
The result is either a mutable object or immutable collections.namedtuple containing
the JSON's dictionary keys as named attributes, and the dictionary values as the
attribute's values - much like javascript !

Example below taken from test.testJsonl.py.testJSONL_examples.test
.
```
#!python
json_data = {'workmen':[{'name':'bob the plumber',
                         'jobs':[123, 124, 125, 200, 211],
                         'clients':{'name':'Mrs. Plumber',
                                    'age': 31,
                                    'occupation':'house wife',
                                    'payments': ['$10.99', '$98.20']
                                   }},
                        ],
             'accounts':1,
             'data':'stuff',
             'private':{'state':'sold'}
             }
#    Create the jsonl object.
data = jsonl.loads(jsonl.dumps(json_data))

#    You can obtain a string or unicode representation of the jsonl object:
a = repr(data)
assert a.startswith('jsonl')
print data
print str(data)
print unicode(data)
#    Now let's play with the data:

#    You can index it alphabetically:
assert data[0]==1
#    or you can access via attributes:
assert data['accounts']==1

#    You can set attributes via an index:
data[0] = 'hello world'
assert data[0] == 'hello world'
assert data['accounts']=='hello world'
#    or via slicing:
data[0:2] = 55      #    Assigns value to attributes 0,1
assert len(data)==4 #    num attributes unchanged.
#    and extended slicing also:
data[0::2] = 123
assert data[0]==123
assert data[1]==55
assert data[2]==123

data = jsonl.loads(jsonl.dumps(json_data))
#    You can slice it to access the values using indexes:
assert data[1:2]==['stuff']
assert data[0:2]==[1, 'stuff']
#    or via keys: start or end
assert str(data['accounts':1])==str([1])
assert str(data[0:'private'])==str([1, 'stuff'])
#    or via keys: start and end
assert str(data['accounts':'private'])==str([1, 'stuff'])
#    Use the slice step parameter as normal (integer or None)
assert data[-3::-1]==['stuff', 1]
print data['private':'data':-1]
assert str(data['private':'data':-1][0])==str(jsonl.loads(jsonl.dumps(json_data['private'])))

#    You can test for a key being 'in' the jsonl object's keys:
assert 'accounts' in data
assert 'doesntexist' not in data

#    You can create an iterator from it:
data = jsonl.loads(jsonl.dumps(json_data))
eKeys = ['accounts', 'data', 'private', 'workmen']
#    This will iterate over the keys:
assert [i for i in iter(data)]==eKeys
#    Create an implicit custom iterator over the keys:
assert [i for i in jsonl.iter(data)]==eKeys
#    Create an explicit custom iterator over the values:
assert [i for i in jsonl.iterkeys(data)]==eKeys
#    Create an explicit custom iterator over the values:
values = [i for i in jsonl.itervalues(data)]
assert values[0] == json_data['accounts']
assert values[1] == json_data['data']
assert jsonl.isEqual(values[2], json_data['private'])
assert jsonl.isEqual(values[3], json_data['workmen'])

#    You can test for equality:
data = jsonl.loads(jsonl.dumps(json_data))
data1 = jsonl.loads(jsonl.dumps(json_data))
#    Using the jsonl object
assert jsonl.isEqual(data, json_data)
assert jsonl.isEqual(data, data1)
#    jsonl.dump
assert jsonl.isEqual(data, jsonl.dumps(json_data))
#    Add an attribute and check for inequality:
data.aaaa = 1
assert not jsonl.isEqual(data, data1)

#    You can delete attributes via an index:
data = jsonl.loads(jsonl.dumps(json_data))
del data[0]
assert data[0] != 'hello world'
assert 'accounts' not in data
try:
    data['accounts']
except IndexError as e:
    assert e.message=='accounts'
else:
    assert False
#    or via keys:
del data['workmen']
assert 'workmen' not in data
try:
    data['workmen']
except IndexError as e:
    assert e.message=='workmen'
else:
    assert False

#    You can get the length (number of keys) in the jsonl object:
data = jsonl.loads(jsonl.dumps(json_data))
assert len(data)==4==len([i for i in iter(data)])

#    You can also check for an accidental circular dependency:
jsonl.verify(data)
#    Add the circular dependency:
data.workmen[0].voo = data
#    And check - catching an exception:
try:
    jsonl.verify(data)
except jsonl.JSONDecodeCircularError:
    assert True
else:
    assert False
#    or without raising an exception
assert not jsonl.verify(data, noraise=True)


In case you're feeling generous:
LTC: LT636SrauWAz9XDz2EKxAXQ5jKqehyhR69
BTC: 13vS6cvzZXf1Yxrar2SYSPQrFLSEwLePV4
```